SUMMARY = "A high-performance, small-footprint platform that implements a subset of HTML5/CSS/JS to run applications, including the YouTube TV app"
HOMEPAGE = "https://cobalt.googlesource.com/"

LICENSE = "BSD-3-Clause & Apache-2.0"
LIC_FILES_CHKSUM = " \
   file://LICENSE;md5=0fca02217a5d49a14dfe2d11837bb34d \
   file://starboard/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57 \
"

# Disable by default
DEFAULT_PREFERENCE_morty = "-1"
TOOLCHAIN = "gcc"

SRC_URI = " \
    git://github.com/youtube/cobalt.git;protocol=https;name=cobalt;branch=master \
    ${CMF_GIT_ROOT}/rdk/components/generic/cobalt;protocol=${CMF_GIT_PROTOCOL};destsuffix=starboard;name=starboard;branch=23.lts.dev \
    file://0001-gn-ensure-we-use-yocto-s-python3-interpreter.patch \
    file://0002-starboard-build-config-allow-GCC-as-a-host-compiler.patch \
    file://0003-starboard-rdk-convert-shared-folder-from-GYP-to-GN.patch \
    file://0004-starboard-rdk-convert-arm-platform-from-GYP-to-GN.patch \
    file://0005-starboard-rdk-shared-player-drop-use-of-deprecated-m.patch \
    file://0006-starboard-rdk-convert-brcm-arm-platform-from-GYP-to-.patch \
    file://0007-starboard-rdk-convert-rpi-platform-from-GYP-to-GN.patch \
"


PR = "1"
SRCREV_cobalt = "db232fd8914af43e3f122ab649319a4302954e47"
SRCREV_starboard = "a78f334ba49c0b2ad33df1f5da05916cea4abe36"
SRCREV_FORMAT = "cobalt_starboard"

do_fetch[vardeps] += " SRCREV_FORMAT SRCREV_cobalt SRCREV_starboard"
S = "${WORKDIR}/git"

DEPENDS += "virtual/egl essos wpeframework-clientlibraries gstreamer1.0 \
            gstreamer1.0-plugins-base python3-native ninja-native bison-native \
            openssl-native python3-six-native python3-urllib3-native ccache-native\
            gn-native"
RDEPENDS_${PN} += "gstreamer1.0-plugins-base-app gstreamer1.0-plugins-base-playback"

TUNE_CCARGS_remove = "-fno-omit-frame-pointer -fno-optimize-sibling-calls"

PACKAGECONFIG ?= "${@bb.utils.contains('DISTRO_FEATURES', 'opencdm', 'opencdm', '', d)}"
PACKAGECONFIG[opencdm] = ""
PACKAGECONFIG[nplb] = ""

inherit pythonnative pkgconfig

COBALT_PLATFORM ?= ""
COBALT_PLATFORM_brcmarm ?= "rdk-brcm-arm"
COBALT_PLATFORM_rpi ?= "rdk-rpi"
COBALT_PLATFORM_arm ?= "rdk-arm"
COBALT_BUILD_TYPE ?= ""${@bb.utils.contains('DISTRO_FEATURES', 'cobalt-qa', 'qa', 'gold', d)}""

do_unpack_extra() {
    bbnote "copy starboard"
    cp -ar "${WORKDIR}/starboard/src/third_party/starboard" "${S}/third_party/"
}
addtask unpack_extra after do_unpack before do_patch

do_configure() {
    export PYTHONPATH="${S}"
    export COBALT_HAS_CRYPTOGRAPHY=0
    export COBALT_HAS_SECURITY_AGENT=0
    export COBALT_HAS_OCDM="${@bb.utils.contains('PACKAGECONFIG', 'opencdm', '1', '0', d)}"
    export COBALT_ARM_CALLCONVENTION="${@bb.utils.contains('TUNE_FEATURES', 'callconvention-hard', 'hardfp', 'softfp', d)}"

    if pkg-config WPEFrameworkCryptography >/dev/null 2>&1; then
        COBALT_HAS_CRYPTOGRAPHY=1
    fi

    if pkg-config securityagent >/dev/null 2>&1; then
        COBALT_HAS_SECURITY_AGENT=1
    fi

    nativepython3 ${S}/cobalt/build/gn.py -c ${COBALT_BUILD_TYPE} -p ${COBALT_PLATFORM}
}

do_compile[progress] = "percent"
do_compile() {
    export PYTHONPATH="${S}"
    export NINJA_STATUS='%p '
    ninja -C ${S}/out/${COBALT_PLATFORM}_${COBALT_BUILD_TYPE} cobalt
}

do_install() {
    install -d ${D}${libdir}
    install -m 0755 ${S}/out/${COBALT_PLATFORM}_${COBALT_BUILD_TYPE}/libcobalt.so ${D}${libdir}

    install -d ${D}${datadir}/content
    cp -arv --no-preserve=ownership ${S}/out/${COBALT_PLATFORM}_${COBALT_BUILD_TYPE}/content ${D}${datadir}/

    # use system provided certs
    rm -rf ${D}${datadir}/content/data/ssl/certs
    mkdir -p ${D}${datadir}/content/data/ssl/
    ln -s /etc/ssl/certs ${D}${datadir}/content/data/ssl/certs
}

# NPLB
do_compile_append() {
    if ${@bb.utils.contains('PACKAGECONFIG', 'nplb', 'true', 'false', d)}; then
        export PYTHONHTTPSVERIFY=0
        ninja -C ${S}/out/${COBALT_PLATFORM}_devel nplb
    fi
}
do_install_append() {
    if ${@bb.utils.contains('PACKAGECONFIG', 'nplb', 'true', 'false', d)}; then
        install -m 0755 ${S}/out/${COBALT_PLATFORM}_devel/nplb ${D}${bindir}
        cp -arv --no-preserve=ownership ${S}/out/${COBALT_PLATFORM}_devel/content ${D}${datadir}/
    fi
}

FILES_${PN}  = "${libdir}/libcobalt.so"
FILES_${PN} += "${datadir}/content/*"

FILES_SOLIBSDEV = ""
INSANE_SKIP_${PN} += "dev-so"
INSANE_SKIP_${PN}-dbg += "dev-so"
