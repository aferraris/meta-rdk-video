SUMMARY = "Generate Ninja"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=0fca02217a5d49a14dfe2d11837bb34d"

TOOLCHAIN = "gcc"

SRC_URI = "git://cobalt.googlesource.com/third_party/gn;protocol=https;branch=main"

SRCREV = "df4a8e68510787e17f87cea99f6a8fe6227fd188"
S = "${WORKDIR}/git"

inherit native

DEPENDS = "ninja-native python3-native"
BDIR = "${S}/out"

do_configure() {
    nativepython3 ${S}/build/gen.py
}

do_compile() {
    ninja -C ${BDIR}
}

do_install_append() {
    install -d ${D}${bindir}
    install -m 0755 ${BDIR}/gn ${D}${bindir}
}

FILES_${PN} = "${bindir}/*"

INSANE_SKIP:${PN} += "already-stripped"
